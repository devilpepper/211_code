#include <cstdio>
#include <cstring>
#include <iostream>
using std::cout;
using std::cerr;
using std::cin;
using std::endl;
#include <fstream>
using std::ifstream;
#include <getopt.h>
#include <stdlib.h> /* atoi function. */
#include <functional>

void try2open(std::ifstream&, const char*, std::ios_base::openmode, const char*, const char*);

int main(int argc, char *argv[])
{
	static const char* usage =
	"Usage: %1$s FILE1 FILE2 [OPTIONS]...\n"
	"Comparison options:\n"
	"   --e                {0}get addresses with equal values (default action).\n"
	"   --ne               {1}get addresses with not equal values.\n"
	"   --gt               {2}get addresses that are greater in FILE1 than in FILE2.\n"
	"   --gte              {3}get addresses that are greater or equal in FILE1 than in FILE2.\n"
	"   --lt               {4}get addresses that are less in FILE1 than in FILE2.\n"
	"   --lte              {5}get addresses that are less or equal in FILE1 than in FILE2.\n"
	"   -c,--compare  NUM  use comparison NUM(see above).\n"
	"Other options:\n"
	"   -b,--bytes         size of data to compare. Powers of 2. Greater than 4 not recommended.\n"
	"                      Enter 0 to enter desired size and addresses at runtime (default is 0)\n"
	"   -i,--interactive   useful if --bytes=0 is given and no session file is available...*\n"
	"   -v,--verbose       outputs to stderr. Useful if stdio is redirected to a session file.\n"
	"   -h,--help          show this message and exit.\n"
	"Accepts input from stdin and outputs to stdio. Recommended use:\n"
	"   %1$s FILE1 FILE2 --COMPARISON_OPTION --bytes=NUM [-v] > SESSIONFILE\n"
	"   %1$s FILE1 FILE2 --COMPARISON_OPTION [-v] < OLD_SESSIONFILE > NEW_SESSIONFILE\n"
	"*Interactive mode is strongly discouraged.\n";

	// define long options
	int cmp = 0, bytes = 0;
	bool interactive = false, verbose = false;
	static struct option long_opts[] = {
		{"e",                no_argument,       &cmp, 0},
		{"ne",               no_argument,       &cmp, 1},
		{"gt",               no_argument,       &cmp, 2},
		{"gte",              no_argument,       &cmp, 3},
		{"lt",               no_argument,       &cmp, 4},
		{"lte",              no_argument,       &cmp, 5},
		{"compare",          required_argument, 0,  'c'},
		{"bytes",            required_argument, 0,  'b'},
		{"interactive",      no_argument,       0,  'i'},
		{"verbose",          no_argument,       0,  'v'},
		{"help",             no_argument,       0,  'h'},
		{0,0,0,0}
	};
	
	//exit if first 2 arguments are not files
	if(argc < 3 || argv[1][0] == '-' || argv[2][0] == '-')
	{
		fprintf(stderr,usage,argv[0]);
		exit(0);
	}

	// process options:
	char c;
	int opt_index = 3;
	
	while ((c = getopt_long(argc, argv, "hvib:c:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				fprintf(stderr,usage,argv[0]);
				exit(0);
			case 'v':
				verbose = true;
				break;
			case 'i':
				interactive = true;
				break;
			case 'b':
				bytes = atoi(optarg);
				break;
			case 'c':
				cmp = atoi(optarg);
				break;
			case '?':
				fprintf(stderr,usage,argv[0]);
				exit(1);
		}
	}
#if 0
	std::binary_function<char, char, bool> cfunc[6] = 
	{
		std::equal_to<char>(),
		std::not_equal_to<char>(),
		std::greater<char>(),
		std::greater_equal<char>(),
		std::less<char>(),
		std::less_equal<char>()
	};
#endif

	//open the files if possible
	ifstream file1, file2;
	try2open(file1, argv[optind], std::ios::binary, usage, argv[0]);
	try2open(file2, argv[optind+1], std::ios::binary, usage, argv[0]);
	if(verbose) cerr<<argv[optind]<<" & "<<argv[optind+1]<<" were opened\n";

	int bites = bytes, OxOFF = 0, here, n;
	char val1[bites], val2[bites];
	if(!bytes)
	{
		if(interactive) cerr<<"Read how many bytes? ";
		cin>>bites;
	}
	cout<<bites<<endl;
	if(verbose) cerr<<"reading "<<bites<<"bytes at a time\n";
	while(cin && file1.good() && file2.good())
	{
		if(!bytes)
		{
			if(interactive) cerr<<"Read from where? ";
			cin>>OxOFF;
			file1.seekg(OxOFF);
			file2.seekg(OxOFF);
			if(verbose) cerr<<"Reading from\t"<<OxOFF<<endl;
		}
		
		memset(val1, 0, bites);
		memset(val2, 0, bites);

		here = file1.tellg();
		file1.read(val1, bites);
		file1.read(val2, bites);
		
		n = memcmp(val1, val2, bites);
		if((cmp==0 && n==0) || (cmp==1 && n!=0) || (cmp==2 && n>0)
				|| (cmp==3 && (n>0 || n==0)) || (cmp==4 && n<0) || (cmp==5 && (n<0 || n==0)))
		{
			cout<<here<<endl;
			if(verbose) cerr<<"true at:\t"<<here<<endl;
		}
	}

	//close the files
	file1.close();
	file2.close();
	return 0;
}

void try2open(std::ifstream& file2open, const char* filename, std::ios_base::openmode mode, const char* usage, const char* argv)
{
	file2open.open(filename, mode);
	if(!file2open.is_open())
	{
		fprintf(stderr, "%s not found\n", filename);
		fprintf(stderr,usage,argv);
		exit(1);
	}
}
