#include <cstdio>
#include <cstring>
#include <iostream>
using std::cout;
using std::cerr;
using std::cin;
using std::endl;
#include <fstream>
using std::ifstream;
#include <getopt.h>
#include <stdlib.h> /* atoi function. */
#include <functional>

void try2open(std::ifstream&, const char*, std::ios_base::openmode, const char*, const char*);

int main(int argc, char *argv[])
{
	static const char* usage =
	"Usage: %1$s FILE [OPTIONS]...\n"
	"General options:\n"
	"   -i,--interactive   useful if --bytes=0 is given and no session file is available...*\n"
	"   -v,--verbose       outputs to stderr. Useful if stdio is redirected to a session file.\n"
	"   -h,--help          show this message and exit.\n"
	"Accepts input from stdin and outputs to stdio. Recommended use:\n"
	"   %1$s FILE [-v] < INPUTFILE > SESSIONFILE\n"
	"*Interactive mode is strongly discouraged.\n";

	// define long options
	bool interactive = false, verbose = false;
	static struct option long_opts[] = {
		{"interactive",      no_argument,       0,  'i'},
		{"verbose",          no_argument,       0,  'v'},
		{"help",             no_argument,       0,  'h'},
		{0,0,0,0}
	};
	
	//exit if first argument is not a file
	if(argc < 2 || argv[1][0] == '-')
	{
		fprintf(stderr,usage,argv[0]);
		exit(0);
	}

	// process options:
	char c;
	int opt_index = 2;
	
	while ((c = getopt_long(argc, argv, "hiv", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				fprintf(stderr,usage,argv[0]);
				exit(0);
			case 'v':
				verbose = true;
				break;
			case 'i':
				interactive = true;
				break;
			case '?':
				fprintf(stderr,usage,argv[0]);
				exit(1);
		}
	}
#if 0
	std::binary_function<char, char, bool> cfunc[6] = 
	{
		std::equal_to<char>(),
		std::not_equal_to<char>(),
		std::greater<char>(),
		std::greater_equal<char>(),
		std::less<char>(),
		std::less_equal<char>()
	};
#endif

	//open the files if possible
	ifstream file1;
	try2open(file1, argv[optind], std::ios::binary, usage, argv[0]);
	if(verbose) cerr<<argv[optind]<<" was opened\n";

	int bytes, OxOFF, n;
	char val1[bytes];
	if(interactive) cerr<<"Read how many bytes? ";
	cin>>bytes;
	
	if(verbose) cerr<<"reading "<<bytes<<"bytes at a time\n";
	while(cin && file1.good())
	{
		if(interactive) cerr<<"Read from where? ";
		cin>>OxOFF;
		file1.seekg(OxOFF);
		if(verbose) cerr<<"Reading from\t"<<OxOFF<<endl;
		
		memset(val1, 0, bytes);
		file1.read(val1, bytes);
		
		cout<<OxOFF<<": "<<val1<<endl;
		if(verbose) cerr<<OxOFF<<": "<<val1<<endl;
	}

	//close the files
	file1.close();
	return 0;
}

void try2open(std::ifstream& file2open, const char* filename, std::ios_base::openmode mode, const char* usage, const char* argv)
{
	file2open.open(filename, mode);
	if(!file2open.is_open())
	{
		fprintf(stderr, "%s not found\n", filename);
		fprintf(stderr,usage,argv);
		exit(1);
	}
}

