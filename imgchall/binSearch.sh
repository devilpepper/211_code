#!/bin/bash

v=""
i=""
b=0
c=0

while getopts "hvib:c:" opt; do
	case "${opt}" in
		h) 
			"$(dirname $0)/binSearch" --help
			exit 1
			;;
		v) 
			v=" --verbose"
			;;
		i) 
			i=" --interactive"
			;;
		b) 
			b=$OPTARG
			;;
		c) 
			c=$OPTARG
			;;
		\?) 
			exit 1
			;;
	esac
done

results="results.txt"
temp="temp.txt"

j=$((OPTIND + 1))

if (( j <= $# ))
then
	"$(dirname $0)/binSearch" "${!OPTIND}" "${!j}"$v$i "--bytes="${b} "--compare="${c} > "$results"
else
	echo "Not enough files"
fi

for ((j=OPTIND+2; j<=$#; j++))
do
	"$(dirname $0)/binSearch" "${!OPTIND}" "${!j}"$v$i "--compare="${c} < "$results" > "$temp"
	rm "$results"
	mv "$temp" "$results"
done
