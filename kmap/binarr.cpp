#include "binarr.h"

binarr::binarr()
{
	this->B = new bool[4];
	this->size = 4;
}
binarr::binarr(int b)
{
	this->B = new bool[b];
	this->size = b;
}
binarr::~binarr()
{
	delete [] this->B;
}

bool& binarr::operator[](int i)
{
	if (size > 0)
		return (this->B[i]);
	else
		return *(this->B);
}

int binarr::toInt()
{
	int toDec = 0;
	for(int i=0; i<this->size; i++) toDec += B[i] << i;
	return toDec;
}

void binarr::printBin()
{
	for (int i = this->size - 1; i >= 0; i--) printf("%d",B[i]);
	printf("\n");
}

void binarr::zero()
{
	for (int i = 0; i < this->size; i++) B[i] = 0;
}