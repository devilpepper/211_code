#include "kmap.h"

#define GREY(x) (x^(x>>1))
#define CELL(x,y,vBits) ((GREY(x)<<vBits)+GREY(y))
#define EXP2(n) (1<<n)


template<>
bool std::operator<(const PairInt& lhs, const PairInt& rhs){
	return ((lhs.first * lhs.second) > (rhs.first * rhs.second))
		|| ((lhs.first * lhs.second) == (rhs.first * rhs.second) && (lhs.first > rhs.first));
}

namespace kmap
{
	std::string pos(std::string* inputs, char* outputs, int numBits)
	{
		return solve(inputs, outputs, numBits, '0');
	}
	std::string sop(std::string* inputs, char* outputs, int numBits)
	{
		return solve(inputs, outputs, numBits, '1');
	}

	std::string solve(std::string* inputs, char* outputs, int numBits, char mTerm)
	{
		printf("solving...\n");
		int vBits = numBits / 2;
		int hBits = vBits + (numBits % 2);
		printf("numBits:%d\nvBits:%d\nhBits%d\n", numBits, vBits, hBits);
		int hBoundary = EXP2(hBits), vBoundary = EXP2(vBits), oneDim = EXP2(numBits);
		printf("hBoundary:%d\nvBoundary:%d\noneDim%d\n", hBoundary, vBoundary, oneDim);
		bool *inTerm = new bool[oneDim];
		printf("init *inTerm\n");
		for (int i = 0; i < oneDim; i++) inTerm[i] = false;
		printf("\n\n\n%d\n", oneDim);

		set<PairInt> R;
		//first: rectangle, second:position
		set<pair<PairInt, PairInt> > mTerms;
		printf("makeRect()\n");
		makeRect(R, hBits, vBits);
		for (std::set<PairInt>::iterator it = R.begin(); it != R.end(); it++)
		{
			//printf("\n%dx%d", it->first, it->second);
			for (int i = 0; i < vBoundary; i++)
				for (int j = 0; j < hBoundary; j++)
				{
					if (!isCovered(inTerm, (*it), j, i, hBoundary, vBits))
					{
						if (isTerm(outputs, (*it), j, i, hBoundary, vBits, mTerm))
						{
							coverRect(inTerm, (*it), j, i, hBoundary, vBits);
							mTerms.insert(make_pair((*it), make_pair(j, i)));
						}
					}
				}
		}
		std::string s = simpleBoolFunc(mTerms, inputs, numBits, hBoundary, vBoundary, mTerm);
		printf("delete[] inTerm;\n");
		//delete [] inTerm;
		printf("wtf?!\n");
		return s;
	}

	char bool2char(bool b){return (b ? '1' : '0');}
	bool isDontCare(char c){ return ((c == 'x') || (c = '-') || (c == 'X')); }
	bool kCompare(char a, char b){return ((a == b) || isDontCare(a) || isDontCare(b));}

	bool isCovered(bool* doneThat, const PairInt& r, int x, int y, int hBound, int vBits)
	{
		bool beenThere = true;
		int vBound = EXP2(vBits);
		for (int i = 0; i < r.first; i++)
			for (int j = 0; j < r.second; j++)
				//printf("isCovered(): %d\n", CELL(((x + i) % hBound), ((y + j) % vBound), vBits));
				beenThere &= doneThat[CELL(((x + i) % hBound), ((y + j) % vBound), vBound)];
		return beenThere;
	}

	bool isTerm(char* outputs, const PairInt& r, int x, int y, int hBound, int vBits, char mTerm)
	{
		bool box = true;
		char c;
		int vBound = EXP2(vBits);
		for (int i = 0; i < r.first; i++)
			for (int j = 0; j < r.second; j++)
			{
				//printf("isTerm(): %d\n", CELL(((x + i) % hBound), ((y + j) % vBound), vBits));
				c = outputs[CELL(((x + i) % hBound), ((y + j) % vBound), vBits)];
				box &= ((c == mTerm) || isDontCare(c));
			}
		return box;
	}

	void makeRect(set<PairInt>& R, int x, int y)
	{
		for (int i = x; i >= 0; i--)
			for (int j = y; j >= 0; j--)
			{
				R.insert(make_pair(1 << i, 1 << j));
				R.insert(make_pair(1 << j, 1 << i));
			}
	}

	void coverRect(bool* mark, const PairInt& r, int x, int y, int hBound, int vBits)
	{
		int vBound = EXP2(vBits);
		for (int i = 0; i < r.first; i++)
			for (int j = 0; j < r.second; j++)
				mark[CELL(((x + i) % hBound), ((y + j) % vBound), vBits)] = true;
				//printf("coverRect(): %d\n", CELL(((x + i) % hBound), ((y + j) % vBound), vBits));
	}

	std::string simpleBoolFunc(set<pair<PairInt, PairInt> >& mTerms, std::string* inputs, int numBits, int hBoundary, int vBoundary, char mTerm)
	{
		for (std::set<pair<PairInt, PairInt> >::iterator it = mTerms.begin(); it != mTerms.end(); it++)
			printf("%dx%d @ (%d, %d)\n", it->first.first, it->first.second, it->second.first, it->second.second);
		return "";
	}
}
