#pragma once
#include<string>
using std::string;
#include<set>
using std::set;
#include<utility>
using std::pair;
using std::make_pair;

typedef  std::pair<int, int> PairInt;

template<>
bool std::operator<(const PairInt&, const PairInt&);

namespace kmap
{
	std::string pos(std::string*, char*, int);
	std::string sop(std::string*, char*, int);
	std::string solve(std::string*, char*, int, char);
	char bool2char(bool);
	bool isDontCare(char);
	bool kCompare(char, char);
	bool isCovered(bool*, const PairInt&, int, int, int, int);
	bool isTerm(char*, const PairInt&, int, int, int, int, char);
	void makeRect(set<PairInt>&, int, int);
	void coverRect(bool*, const PairInt&, int, int, int, int);
	std::string simpleBoolFunc(set<pair<PairInt, PairInt> >&, std::string*, int, int, int, char);
}
