#pragma once
#include<vector>
class binarr
{
	private:
		bool *B;
		int size;
	public:
		binarr();
		binarr(int b);
		~binarr();
		bool& operator[](int i);
		int toInt();
		void printBin();
		void zero();
};
