#include<iostream>
using std::cin;
using std::cout;
#include<string>
using std::string;
#include<sstream>
using std::stringstream;
#include <stdlib.h> /* atoi function. */
#include <getopt.h>
#include<vector>
using std::vector;

#include "binarr.h"
#include "kmap.h"

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"General options:\n"
"   -i,--in     FILE   read graph from FILE.\n"
"   -o,--out    FILE   use FILE for output.\n"
"   --undir            interpret edge list as an undirected graph.\n"
"   -h,--help          show this message and exit.\n"
"Traversal / search options:\n"
"   --bfs              perform breadth-first search.\n"
"   --dfs              perform depth-first search.\n"
"   --components       compute connected components.\n"
"   --source      NUM  source vertex for traversal/search.\n"
"   --path-to     NUM  find a shortest path to vertex NUM.\n";

int main(int argc, char *argv[])
{
		// define long options
	int circuit = 0, pos = 0, sop = 0, inputs = 4;
	static struct option long_opts[] = {
		{"circuit",    no_argument,       &circuit, 1},
		{"pos",        no_argument,       &pos, 1},
		{"sop",        no_argument,       &sop, 1},
		{"help",       no_argument,       0, 'h'},
		{"inputs",     required_argument, 0, 'i'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;

	while ((c = getopt_long(argc, argv, "hi:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 'i':
				inputs = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	string s;
	stringstream ss;
	getline(cin, s);
	ss.str(s);

	//get input names
	string *X = new string[inputs];
	for(int i=inputs-1; i>=0; i--) ss>>X[i];
	//get output names
	vector<string> Y;
	while(ss>>s) Y.push_back(s);
	bool b;
	//calculate number of possible inputs
	int two2theN = 1<<inputs, numOuts = Y.size(), tblIndexR, tblIndexC;
	//the outputs side of the table.
	//transposed for easier traversal
	char **outputs = new char*[numOuts];
	for(int i=0; i<numOuts; i++) outputs[i] = new char[two2theN];
	binarr inVal(inputs);

#if 0
	cout << inVal.toInt() << "\n";
	for (int i = 0; i < inputs; i++) inVal[i] = 0;
	cout << inVal.toInt() << "\n";
	for (int i = 0; i < inputs; i++) inVal[i] = 1;
	cout << inVal.toInt() << "\n";
#endif

	//process table
	while(getline(cin, s))
	{
		ss.clear();
		ss.str(s);
		//cout << s << std::endl;
		//cout << ss.str() << std::endl;
		//inVal.zero();
		//inVal.printBin();
		for (int i = inputs - 1; i >= 0; i--) {
			ss >> inVal[i];
			//inVal.printBin();
		}

		//cout << ss.str() << std::endl;
		tblIndexC = inVal.toInt();
		//cout << tblIndexC << "\n";
		tblIndexR = 0;

		while(ss >> b)
		{
			outputs[tblIndexR][tblIndexC] = b;
			tblIndexR++;
		}

	}
#if 0
	//see output table
#if 0
	for (int i = 0; i < numOuts; i++)
	{
		for (int j = 0; j < two2theN; j++)
			cout << outputs[i][j] << "\t";
		cout << "\n";
	}
#else
	//easier to read untransposed
	for (int i = 0; i < two2theN; i++)
	{
		for (int j = 0; j < numOuts; j++)
			cout << outputs[j][i] << "\t";
		cout << "\n";
	}
#endif
#endif
	//at this point, outputs[][j] contains the outputs
	//for when the inputs X_{n-0} = j.
	//The second dimension corresponds to individual outputs;
	//i.e. For seven segment display, outputs[0][j] may 
		//may correspond with segment A's output when the input is j.
	
	//one segment at a time
	for(int i=0; i<numOuts; i++)
	{
		cout << Y[i] << "\n" << i << "\n";
		if (pos) cout << "\n\t" << kmap::pos(X, outputs[i], inputs);
		if (sop) cout << "\n\t" << kmap::sop(X, outputs[i], inputs);
		cout << "\n\n";
	}

	delete[] X;
	for (int i = 0; i < numOuts; i++) delete[] outputs[i];
	delete[] outputs;
	return 0;
}
