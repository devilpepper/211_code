#include<iostream>
using std::cout;

//outputs a semi-colon delimited truth table
//to be imported into a spreadsheet

int main()
{
	//Just need AND, NAND, and NOR for this
	//The rest are just for future referenece
	char AND[4] = "\u22C5",
		 //OR = '+',
		 //XOR[4] = "\u2295",
		 //NOT = '~',
		 NAND = '|',
		 NOR = '-';
		 //XNOR[4] = "\u29BF";
	//Truth table variables
	int a,b,c,d;

	//Header
	cout<<"A;B;C;D;A"<<AND<<"B;C"<<NAND<<"D;"
		<<"(A"<<AND<<"B)"<<NOR<<"(C"<<NAND<<"D);\n";
	//data
	for(a=0; a<2; a++)
		for(b=0; b<2; b++)
			for(c=0; c<2; c++)
				for(d=0; d<2; d++)
					cout<<a<<';'<<b<<';'<<c<<';'<<d<<';'
						<<(a && b)<<';'<<(!(c && d))<<';'
						<<(!((a && b) || (!(c && d))))<<";\n";
	return 0;
}
